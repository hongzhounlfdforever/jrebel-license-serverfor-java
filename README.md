# Jrebel Java 热部署服务项目

#### JRebel激活服务地址免费获取：关注公众号【猿码天地】 回复 ***002*** 获取

#### [点击获取Java技术栈学习资源](https://gitee.com/zhangbw666/it-knowledge)

>知识改变未来，很多人不相信，是因为他们从来没经历过，或者在他们周围没遇到过，无法改变自己的认知。只有某一天发现自己的一点知识成果的付出得到超期的回报时，才体会，才努力花大量成本去弥补其先前的光阴。这是当下人的矛盾与现状。而那些早已成功的人，在很早就已经知道这个太简单的道理，所以，努力付诸行动不晚。——《送给程序员朋友的一段话》

A license server for Jrebel & JetBrains products, it also support JRebel for Android and XRebel.

***

NOTE: This is provided for educational purposes only. Please support genuine.
***
## 1.运行部署
1.1 本地运行
```
cd /path/to/project
mvn compile 
mvn exec:java -Dexec.mainClass="com.vvvtimes.server.MainServer" -Dexec.args="-p 8081"
```
1.2 打成jar包（Windows或Linux）运行
```
mvn package  
java -jar JrebelBrainsLicenseServerforJava-1.0-SNAPSHOT.jar -p 8081
或者
nohup java -jar JrebelBrainsLicenseServerforJava-1.0-SNAPSHOT.jar -p 8081 >/dev/null 2>&1 &
```
默认端口 8081

1.3 使用 gradle 运行
```
gradle shadowJar
java -jar JrebelBrainsLicenseServerforJava-1.0-SNAPSHOT.jar -p 8081
```
## 2.Docker部署
Build image
```
mvn package 
docker build -t jrebel-ls
```

start container
```
docker run -d --name jrebel-ls --restart always -e PORT=9001 -p 9001:9001 jrebel-ls
```
默认端口 8081

## 3.支持软件

Jrebel

JRebel for Android

XRebel

JetBrains Products

## 4.问题反馈

+ 关注公众号 【猿码天地】 私信 ***007*** 解答相关问题  
<p align="left">
    <img src="https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg" width="">
</p>